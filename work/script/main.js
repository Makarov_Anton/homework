$(document).ready(function () {
    function t(t) {
        0 > t && (t = 0);
        var e = Math.floor(t / 60 / 60 / 24), a = Math.floor(t / 60 / 60), r = Math.floor((t - 60 * a * 60) / 60),
            i = Math.floor(t - 60 * a * 60 - 60 * r), o = Math.floor((t - 24 * e * 60 * 60) / 60 / 60);
        String(o).length > 1 ? $(".hour").text(o) : $(".hour").text("0" + o), String(r).length > 1 ? $(".min").text(r) : $(".min").text("0" + r), String(i).length > 1 ? $(".sec").text(i) : $(".sec").text("0" + i)
    }

    var e = 10768;
    setInterval(function () {
        e -= 1, t(e)
    }, 1e3), $(".gallery .owl-carousel").owlCarousel({
        items: 1,
        loop: !0,
        nav: !0,
        navText: !1,
        margin: 10
    }), jQuery("[data-scroll]").click(function () {
        var t = jQuery(this).attr("data-scroll"), e = jQuery(t).offset().top;
        jQuery("body,html").animate({scrollTop: e}, 1500)
    }), $(".sertificats .owl-carousel").owlCarousel({
        items: 3,
        loop: !0,
        nav: !0,
        navText: !1,
        responsive: {
            0: {items: 1},
            500: {items: 2, margin: 30},
            700: {items: 2, margin: 30},
            960: {items: 3, margin: 30},
            1024: {items: 3, margin: 30},
            1920: {items: 3, margin: 30}
        }
    }), $(".opinions .owl-carousel").owlCarousel({
        items: 1,
        loop: !0,
        nav: !0,
        navText: !1
    }), $(".request ul li .title").click(function () {
        var t = $(this).parent().attr("data-active");
        void 0 == t || "0" == t ? ($(".request ul li").removeAttr("data-active"), $(this).parent().attr("data-active", "1")) : $(this).parent().attr("data-active", "0")
    }), $('input[type="text"],textarea').click(function () {
        $(this).attr("placeholder", "")
    }), $("input[name=name]").blur(function () {
        $(this).attr("placeholder", "Имя")
    }), $('input[type="tel"]').mask("+7 (999) 999-99-99"), $("[data-modal]").click(function () {
        var t = $(this).attr("data-modal");
        $(t).arcticmodal()
    }), jQuery("form .button-pink").on("click", function () {
        var t = (jQuery(this).attr("data-item"), jQuery(this).parent().serialize()),
            e = (jQuery(this).parent().parent(), jQuery(this).parent().find(".required").length);
        jQuery(this).parent().find(".required").each(function () {
            "" != jQuery(this).val() && (e -= 1)
        }), 0 == e ? jQuery.ajax({
            type: "POST", url: "form.php", data: t, success: function () {
                swal("Отлично!", "Ваша заявка успешно отправлена, мы свяжемся с вами в самое ближайшее время, спасибо :)!", "success"), $('input[type="text"],input[type="tel"]').val(""), yaCounter42620519.reachGoal("order"), $.arcticmodal("close")
            }, error: function () {
                sweetAlert("Ошибка!", "Форма не отправлена, пожалуйста обновите страницу и попробуйте снова!", "error")
            }
        }) : sweetAlert("Ошибка!", "Заполните все поля правильно!", "error")
    }), /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || (wow = new WOW({
        boxClass: "wow",
        animateClass: "animated",
        offset: 0,
        mobile: !1,
        live: !0
    }), wow.init())
});