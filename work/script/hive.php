<?php
	/**
	* api-client.php : zeOn
	* Copyright (c) 2012 - 2016 Oleg Shteinliht (fish9370@mail.ru),
	* Russian Federation, Moscow
	**/

	class api {
		var $cell;
		var $secret;
		var $url;

		var $curl;
		var $ctype;		/* content type */
		var $len;		/* content length */
		var $head;		/* http header */
		var $content;		/* content */

		var $result;		/* php array result */
		var $last_error;

		function api($cell, $secret, $url) {
			$this->cell = $cell;
			$this->secret = $secret;
			$this->url = $url;
			$this->curl = curl_init($url);
			curl_setopt($this->curl, CURLOPT_POST, true);
			curl_setopt($this->curl, CURLOPT_HEADER, true);
			curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
		}

		function query($post) {
			$post['hash'] = md5(http_build_query($post) . $this->secret);
			curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query($post));	

			$response = curl_exec($this->curl);
			$this->ctype = curl_getinfo($this->curl, CURLINFO_CONTENT_TYPE);
			$this->head = substr($response, 0, curl_getinfo($this->curl, CURLINFO_HEADER_SIZE));
			$this->content = substr($response, curl_getinfo($this->curl, CURLINFO_HEADER_SIZE));
			$this->len = curl_getinfo($this->curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD);

			switch ($this->ctype) {
				case 'application/json':
					$content = json_decode($this->content, true);

					if (isset($content['code']) && $content['code'] == 'fail') {
						$this->last_error = $content['text'];

						return FALSE;
					}
	
					$this->result = $content;
					return TRUE;

				case 'audio/wav':
				case 'image/tiff':
				case 'application/octet-stream':
					return TRUE;
			}
		
			return FALSE;
		}

		function close() {
			curl_close($this->curl);
		}

		function get_filename() {
			if (!preg_match("/^Content-disposition: .*?filename=(.*)$/m", $this->head, $match)) {
				$this->last_error = 'filename is not assigned';
				return FALSE;
			}

			$filename = trim($match[1]);

			if (strpos($filename, "..") !== FALSE) {
				$this->last_error = 'hack attempt';
				return FALSE;
			}

			return $filename;
		}

		function get_calls($start, $end, $limit = 0, $disposition = 'any', $calltype = 'any') {
			$post = array(
				'cell' => $this->cell,
				'method' => 'get-calls',
				'start' => $start,
				'end' => $end,
				'limit' => $limit,
				'disposition' => $disposition,
				'calltype' => $calltype
			);

			return $this->query($post);
		
		}

		function get_points($state = false) {
			$post = array(
				'cell' => $this->cell,
				'method' => 'get-points',
			);
		
			return $this->query($post);
		}

		function get_trunks($state = false) {
			$post = array(
				'cell' => $this->cell,
				'method' => 'get-trunks',
			);
		
			return $this->query($post);
		}

		function get_filesize($link) {
			$post = array(
				'cell' => $this->cell,
				'method' => 'get-filesize',
				'link' => $link
			);
		
			return $this->query($post);
		}

		function get_file($link, $path) {
			$post = array(
				'cell' => $this->cell,
				'method' => 'get-file',
				'link' => $link
			);

			if (empty($path)) {
				$this->last_error = 'empty path';
				return FALSE;
			}

			if (($ret = $this->query($post)) && ($filename = $this->get_filename())) {
				file_put_contents($path . '/' . $filename, $this->content);

				return TRUE;
			}

			return FALSE;
		}

		function set_subscription($url, $event){
			$post = array(
				'cell' => $this->cell,
            			'method' => 'subscribe',
				'event' => $event,
	            		'url' =>  $url,
			);
			return $this->query($post);
		}

		function get_callee($caller){
			$post = array(
				'cell' => $this->cell,
            			'method' => 'get-callee',
		    		'caller' => $caller,
			);
			return $this->query($post);
		}

		function get_caller($callee){
			$post = array(
				'cell' => $this->cell,
        	                'method' => 'get-caller',
			        'callee' => $callee,
			);
			return $this->query($post);
		}
		
		function call_me($src, $dst, $to_num = FALSE){
			$post = array(
				'cell' => $this->cell,
        	    'method' => 'call-me',
			    'src' => $src,
			    'dst' => $dst,
			);
			if ($to_num)
			    $post['to-number'] = 1;
			return $this->query($post);
		}

	}
//
//    $cell = 1214;
//    $secret = '316F796inXZA11NTM';
//
//    $api = new api($cell, $secret, 'https://hive.iptelefon.su/hive/api/');
//    if($a
//pi->call_me($_POST['src'], $_POST['dst'], TRUE))
//        print_r($api->result);
//	else
//        echo $api->last_error;


?>