<?php
session_start();
if (!isset($_SESSION['login'])){
  header("Location: login.php");
}
require_once "function.php";
$isbn = "";
$name = "";
$author = "";

try
{

  $db = dbConnect();
  $value = "";
  $get = addslash($_GET);

  $sth = $db->prepare("select id from user WHERE login = '{$_SESSION['login']}'");
  $sth->execute();
  $user = $sth->fetch(PDO::FETCH_ASSOC);

  if (isset($get['action'])) {

    switch ($get['action']) {
      case "edit":
        $sqlEdit = ("select description from task WHERE id = {$get['id']}");

        if ($res = $db->query($sqlEdit)) {
          foreach ($res as $row) {
            $value = $row['description'];
          }
        }
        break;

      case "done":
        $data['is_done'] = 1;
        $sqlDone = ("update task SET is_done = 1 WHERE id = {$get['id']}");
        $db->exec($sqlDone);
        break;

      case "delete":
        $sqlDelete = ("DELETE FROM task WHERE id = {$get['id']}");
        $db->exec($sqlDelete);
        break;
    }
  }

  $post = addslash($_POST);

  if (isset($post['description'])) {
    if (strlen($post['description']) < 4) {
      echo "Количество симвлов должно быть больше 3!";
    } else {
      if (isset($get['action']) && $get['action'] == "edit") {
        $sql = ("update task SET description = '{$post['description']}' where description = '{$value}' AND id = {$get['id']}");
        $db->exec($sql);
        $value = "";
        header("Refresh:0, url=index.php");
      }else{
        $sqlInsert = ("INSERT INTO task (description, date_added, user_id, assigned_user_id) VALUE ('{$post['description']}', NOW(),'{$user['id']}', {$user['id']})");
        $db->exec($sqlInsert);
      }

    }
  }

  if (isset($post['assigned_user_id'])) {
    $assigned_task = explode(";", $post['assigned_user_id']);
    $task_id = $assigned_task[0];
    $assigned_user = $assigned_task[1];

    $result = $db->query("SELECT id FROM user WHERE login = '{$assigned_user}'");
    foreach ($result as $row) {
      $db->query("UPDATE task SET assigned_user_id = '{$row['id']}' where id = '{$task_id}'");
    }
  }
  $sql_author = "SELECT u2.id AS user_id, task.id, description, is_done, date_added, u1.login AS author, u2.login AS assigned FROM `task` LEFT JOIN `user` AS u1 ON u1.id=task.user_id LEFT JOIN `user` AS u2 ON u2.id=task.assigned_user_id where u1.login = '{$_SESSION['login']}'";
  $sql_assigned = "SELECT u2.id AS user_id, task.id, description, is_done, date_added, u1.login AS author, u2.login AS assigned FROM `task` LEFT JOIN `user` AS u1 ON u1.id=task.user_id LEFT JOIN `user` AS u2 ON u2.id=task.assigned_user_id where u2.login = '{$_SESSION['login']}' and u1.login != '{$_SESSION['login']}';";

  if (isset($post['sort_by'])) {
    $sql_author = sort_print_task($post, $sql_author);
  }

  $data = print_task($sql_author, $db);
  $datas = print_task($sql_assigned, $db);


  $sql = "select login from user";
  if ($result = $db->query($sql)) {
    foreach ($result as $row) {
      $users[] = $row['login'];
    }
  }
}
catch (PDOException $e) {
  die("Error: " . $e->getMessage());
}
?>

<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
  <title></title>

  <style>
    table {
      border-spacing: 0;
      border-collapse: collapse;
    }

    table td, table th {
      border: 1px solid #ccc;
      padding: 5px;
    }

    table th {
      background: #eee;
    }
  </style>

</head>
<body>

<h1>Добро пожаловать, <?= $_SESSION['login'] ?>. Твой список дел на сегодня.</h1>
<div style="float: left">
  <form method="POST">

    <input type="text" name="description" placeholder="Описание задачи" value="<?= $value ?>">
    <input type="submit" name="save" value="Добавить">

  </form>
</div>
<div style="float: left; margin-left: 20px;">
  <form method="POST">
    <label for="sort">Сортировать по:</label>
    <select name="sort_by">
      <option value="date_created">Дате добавления</option>
      <option value="is_done">Статусу</option>
      <option value="description">Описанию</option>
    </select>
    <input type="submit" name="sort" value="Отсортировать"/>
  </form>
</div>
<div style="clear: both"></div>

<table>
  <tr>
    <th>Описание задачи</th>
    <th>Дата добавления</th>
    <th>Статус</th>
    <th></th>
    <th>Ответственный</th>
    <th>Автор</th>
    <th>Закрепить задачу за пользователем</th>
  </tr>
  <tr>
      <?php
      if (empty($data)){
        echo "Дел пока нет, кайфуй.";
      }else{
      foreach ($data as $date){

      $status = ($date['is_done'] == 0) ? "<span style='color: orange;'>В процессе</span>" : "<span style='color: green;'>Выполнено</span>";
      ?>
      <?php if (isset($date['description']) && strlen($date['description']) > 3){ ?>
  <tr>
    <td><?= $date['description'] ?></td>
    <td><?= $date['date_added'] ?></td>
    <td><?= $status ?></td>
    <td>
      <a href='?id=<?= $date['id'] ?>&action=edit'>Изменить</a>
      <a href='?id=<?= $date['id'] ?>&action=done'>Выполнить</a>
      <a href='?id=<?= $date['id'] ?>&action=delete'>Удалить</a>
    </td>
    <td><?= $date['assigned'] ?></td>
    <td><?= $date['author'] ?></td>
    <td>
      <form method="POST">
        <select name="assigned_user_id">
          <?php foreach ($users as $user) { ?>
            <option value="<?= "{$date['id']};{$user}" ?>"><?= $user ?></option>
          <?php } ?>
        </select>
        <input type="submit" name="assign" value="Переложить ответственность">
      </form>
    </td>
  </tr>
  </tr>
  <?php }}} ?>

</table>
<div>
  <h3>Также, посмотрите, что от Вас требуют другие люди:</h3>
</div>
<table>
  <tr>
    <th>Описание задачи</th>
    <th>Дата добавления</th>
    <th>Статус</th>
    <th></th>
    <th>Ответственный</th>
    <th>Автор</th>
  </tr>
  <tr>
    <?php
    if (empty($datas)){
      echo "Дел пока нет, кайфуй.";
    }else{
    foreach ($datas as $dates){
      $status = ($dates['is_done'] == 0) ? "<span style='color: orange;'>В процессе</span>" : "<span style='color: green;'>Выполнено</span>";
      ?>
      <?php if (isset($dates['description']) && strlen($dates['description']) > 3){ ?>
    <tr>
      <td><?= $dates['description'] ?></td>
      <td><?= $dates['date_added'] ?></td>
      <td><?= $status ?></td>
      <td>
        <a href='?id=<?= $dates['id'] ?>&action=edit'>Изменить</a>
        <a href='?id=<?= $dates['id'] ?>&action=done'>Выполнить</a>
        <a href='?id=<?= $dates['id'] ?>&action=delete'>Удалить</a>
      </td>
      <td><?= $_SESSION['login'] ?></td>
      <td><?= $dates['author'] ?></td>
    </tr>
    </tr>
  <?php }}} ?>

</table>

<div style="font-weight: bold; font-size: 14px; color: #1e7e34; margin: 50px 0;">
  <a href="logout.php">Выйти</a>
</div>
</body>
</html>
