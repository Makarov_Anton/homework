<!---->
<?php
///*
// * Плюсы объектов
// * 1. очень удобно и быстро описывать какую либо сущность. С конструктором код уменьшается в разы. Всегда можно обратится к классу.
// * Минусы объектов
// * 1. Пока не обнаружил. всё нравиться.
// */
//classes Car
//{
//  public $wheel;
//  public $carColor;
//  public function standard()
//  {
//    echo "Стандартная сборка этой марки {$this->wheel} колеса и {$this->carColor} цвет машины.<br>";
//  }
//  public function __construct($carColorConst = "yellow", $wheelConst = 4)
//  {
//    $this->carColor = $carColorConst;
//    $this->wheel = $wheelConst;
//  }
//}
//
//$bmw = new car();
//$bmw->standard();
//echo "<br>{$bmw->wheel} <br> {$bmw->carColor}<br><br>";
//
//$mercedes = new car("white", 5);
//$mercedes->standard();
//$mercedes->wheel = 7;
//echo "<br>{$mercedes->wheel} <br>{$mercedes->carColor}<br><br>";
//
//classes Television
//{
//  public $antenna = 1;
//  public $channels = 50;
//  public function standard()
//  {
//    echo "Стандартная сборка этой марки антены {$this->antenna}  и {$this->channels} цвет.<br>";
//  }
//}
//
//$panasonic = new Television();
//$panasonic->channels = 100;
//$panasonic->standard();
//
//$toshiba = new Television();
//$toshiba->antenna = 2;
//$toshiba->standard();
//
//classes BallpointPen
//{
//  public $penColor = "red";
//  public $inkColor = "black";
//  public function standard()
//  {
//    echo "Стандартная сборка этой марки {$this->penColor} цвет ручки и {$this->inkColor} цвет чернил .<br>";
//  }
//}
//
//$dalsey = new BallpointPen();
//
//$dalsey->penColor = "yellow";
//$dalsey->inkColor = "red";
//$dalsey->standard();
//
//$lamy = new BallpointPen();
//$lamy->standard();
//
//classes Duck
//{
//  public $wings = 2;
//  public $duckColor;
//  public function standard()
//  {
//    echo "Летят утки<br>";
//  }
//}
//
//$duck = new Duck();
//$duck->standard();
//echo $duck->duckColor = "black<br>";
//
//$daffiduck = new Duck();
//echo $daffiduck->wings = 0 . "крыльев, зато 2 руки<br>";
//
//
//classes Product
//{
//  public $price;
//  public $type;
//
//  public function __construct($price = 200, $type = "продукты")
//  {
//    $this->price;
//    $this->type;
//  }
//}
//
//$banana = new Product();
//$banana->price = 300;
//echo "Цена банана - {$banana->price}<br>";
//echo "Банан - это {$banana->type}";
//
//$apple = new Product();
//$apple->price = 300;
//echo "Цена банана - {$apple->price}<br>";
//echo "Банан - это {$apple->type}<br>";

class Article
{
  private $header = "Header";
  private $article = "Article";
  private $img = "path_img";

  public function setArticle($header, $article ,$img)
  {

    $this->header = $header;
    $this->article = $article;
    $this->img = $img;
  }

  public function getHeader()
  {
    return $this->header;
  }

  public function getArticle()
  {
    return $this->article;
  }

  public function getImg()
  {
    return $this->img;
  }
}

$newNews = new article();
$content = file_get_contents(__DIR__ . "/article.json");
$articles = json_decode($content, true);

class Comments
{

}
?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
</head>
<body>
<?php foreach ($articles as $art){
  $newNews->setArticle($art['header'], $art['article'], $art['img']);?>
<div style="border: 2px solid black; width: 600px; text-align: center;float: left">
  <h1><?= $newNews->getHeader(); ?></h1>
  <img src="<?= $newNews->getImg(); ?>" alt="Картинка" style="float: left; width: 300px; height: 217px; margin: 0 20px">
  <p><?= $newNews->getArticle(); ?></p>
</div>
<?php }?>
</body>
</html>

