<?php
$path = __DIR__ . "/test/";
if(!isset($_GET['name']) || !file_exists($path . $_GET['name'])) {
  header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true);
  exit;
}else{
  $test = $_GET['name'];
}

$content = file_get_contents($path . $test);
$date = json_decode($content, true);

$answer = $_POST;
$i = 0;
if (isset($answer['names'])){
  foreach ($date as $values){
    if ($values['true'] == $answer[$values['name']]) {
      $i++;
    }
  }
  $ball_d = $i + 1;
  $text = $answer['names'] . ", тест пройден!";
  $ball = "Ваша отцена за пройденый тест: " . $ball_d;
  $image = imagecreatetruecolor(522, 738);
  $backcolor = imagecolorallocate($image, 255, 224, 221);
  $textcolor = imagecolorallocate($image, 50, 50, 50);
  $boxFile = __DIR__ . '/img/sert.png';
  $fontFile = __DIR__ . '/img/arial.ttf';
  $imBox = imagecreatefrompng($boxFile);
  imagecopy($image, $imBox, 0, 0, 0, 0, 522, 700);
  imagettftext($image, 20, 0, 120, 330, $textcolor, $fontFile, $text);
  imagettftext($image, 16, 0, 80, 402, $textcolor, $fontFile, $ball);
  header('Content-type: image/png');
  imagepng($image);
  imagedestroy($image);
}
?>

<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <title>address book</title>
  <link rel="stylesheet" href="../../css/new.css">
</head>
<body>

<section>
  <div style="margin: 20px auto; text-align: center; font-weight: bold; font-size: 18px; color: #1e7e34">
    <a href="admin.php" style="float: left;margin-left: 200px;">Загрузить новые тесты</a>
    <a href="list.php" style="margin-left: 150px;">К загруженным тестам!</a>
  </div>
  <form action="" method="POST">
    <input name="names" placeholder="Введите ваше имя" required autofocus>
    <?php foreach ($date as $dates) { ?>
    <?php print_r($dates);?>
    <fieldset>
      <legend><?= $dates['question'] ?></legend>
        <?php foreach ($dates['answer'] as $value) { ?>
      <label><input type="radio" name="<?= $dates['name'] ?>" value="<?= $value?>"
                    required><?= $value ?></label>
        <?php } ?>
    </fieldset>
    <?php } ?>
    <input type="submit" value="Отправить" id="button" ">
  </form>
</section>
</body>
</html>
