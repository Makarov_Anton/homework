<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class SectorController extends Controller
{
  public function getData()
  {
    $data = User::all();
    return view('welcome', ['data' => $data]);
  }

  public function create()
  {
    if (!empty($_GET['name']) && !empty($_GET['surname']) && !empty($_GET['patronymic']) && !empty($_GET['phone'])) {
      $newLine = User::create($_GET);
      return redirect('/');
    } elseif (!empty($_GET['name']) || !empty($_GET['surname']) || !empty($_GET['patronymic']) || !empty($_GET['phone'])) {
      echo "Заполните все поля формы";
    } else {
      return view('create');
    }
  }

  public function update()
  {
    $lineUpdate = User::find($_GET['id']);
    return view('update',
      ['surname' => $lineUpdate->surname,
        'name' => $lineUpdate->name,
        'patronymic' => $lineUpdate->patronymic,
        'phone' => $lineUpdate->phone,
        'id' => $lineUpdate->id,
      ]);
  }

  public function updateLine()
  {
    if (!empty($_GET['name']) && !empty($_GET['surname']) && !empty($_GET['patronymic']) && !empty($_GET['phone'])) {
      $user = User::find($_GET['id']);
      $user->name = $_GET['name'];
      $user->surname = $_GET['surname'];
      $user->patronymic = $_GET['patronymic'];
      $user->phone = $_GET['phone'];
      $user->save();
      return redirect('/');
    } else {
      echo "Заполните все поля формы";
    }
  }
}
