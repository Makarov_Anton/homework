@extends('layouts.app')
@section('container')
    <h2>Пожалуйста, отредактируйте нужные Вам данные</h2>
    <form action="/update/line">
        Фамилия
        <input class="input_create" type="text" name="surname" value="{{ $surname }}"><br>
        Имя
        <input class="input_create" class="input_create type="text" name="name" value="{{ $name }}"><br>
        Отчество
        <input class="input_create" type="text" name="patronymic" value="{{ $patronymic }}"><br>
        Телефон
        <input class="input_create" type="text" name="phone" value="{{ $phone }}"><br>
        <input class="input_create" type="hidden" name="id" value="{{ $id }}"><br>
        <input class="input_create" type="submit" value="обновить запись">
    </form>
@endsection