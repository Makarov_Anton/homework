@extends('layouts.app')
@section('container')
    <h2>Введите данные новой записи</h2>
    <form action="/create">
        Фамилия
        <input class="input_create" type="text" name="surname"><br>
        Имя
        <input class="input_create" type="text" name="name"><br>
        Отчество
        <input class="input_create" type="text" name="patronymic"><br>
        Телефон
        <input class="input_create" type="text" name="phone"><br>
        <input class="input_create" type="hidden" name="id"><br>
        <input class="input_create" type="submit" value="добавить запись">
    </form>
@endsection