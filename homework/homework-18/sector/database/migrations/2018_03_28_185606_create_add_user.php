<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      \DB::table('users')->insert([
        'id' => 1,
        'surname' => 'Давыдов',
        'name' => 'Аристарх',
        'patronymic' => 'Кузьмич',
        'phone' => '79998887766'
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      \DB::table('users')->where('id', '=', 1)->delete();
    }
}
