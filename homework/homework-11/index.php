<?php
/*
 * 1.Пространство имён(namespace) - это некоторое хранилище, созданное для абстрактной группировки уникальных идентификаторов(имён).
 * Если использовать пространства имён, то можно подключать сторонние библиотеки и не бояться, что там будут такие же имена, как и в нашем коде
 *
 * 2.Исключение – это объект, являющийся экземпляром встроенного класса Exception.
 * Этот объект создаётся для хранения информации о произошедшей ошибке и для вывода сообщений о ней.
 * - Исключенияпозволяют нам определять логику их обработки в зависимости от контекста.
 *
 *
 */
require_once __DIR__ . "/autoloader.php";


$bag = new \classes\Bag();
$apples = new \classes\Fruits("Apples", "Fruits.class", 210, 10, 5);
$apples->getDiscount();
$bag->add($apples);
$potato = new \classes\Vegetables("Potato", "Vegetables.class", 150, 5, 2);
$bag->add($potato);
$apples = new \classes\Fruits("Apples", "Fruits.class", 210, 10, 2);
$apples->getDiscount();
$bag->add($apples);
$Order = new \classes\Order();
$Order->print($bag);
$Order->quantity($bag);
$Order->total($bag);
//$bag->delete("Apples"); //delete
//$bag->emptyBag(); //clear

