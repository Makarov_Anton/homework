<?php

function autoload($className)
{
  $filePath = __DIR__. "\\" . str_replace('\\', DIRECTORY_SEPARATOR, $className . '.class.php');
  if (file_exists($filePath)){
    include "$filePath";
  }
}
function autoloadOther($className)
{
  $filePath = __DIR__. "\\" . str_replace('\\', DIRECTORY_SEPARATOR, $className . '.php');
  if (file_exists($filePath)){
    include "$filePath";
  }
}
spl_autoload_register('autoload');
spl_autoload_register('autoloadOther');



