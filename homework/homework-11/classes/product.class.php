<?php

namespace classes;



abstract class Product implements \interfaces\methods
{
  public $price;
  public $category;
  public $discount;
  public $name;
  public $delivery;
  public $quantity;
  public $subtotal;

  abstract public function getDescription();

  public function __construct($name, $category, $price, $discount = 0, $quantity = 1)
  {
    $this->name = $name;
    $this->category = $category;
    $this->price = $price;
    $this->discount = $discount;
    $this->quantity  = $quantity;

     if ($this->discount){
       $this->price = round($this->price - ($this->price / $this->discount), 2);
     }
  }

  public function getDiscount()
  {
    if ($this->discount) {
      return round($this->price - ($this->price / $this->discount), 2);
    } else {
      return $this->price;
    }
  }

  public function getDelivery()
  {
    if ($this->discount){
      return $this->delivery = 300;
    }else{
      return $this->delivery = 250;
    }
  }

  public function subtotal()
  {
    $this->subtotal = $this->price*$this->quantity;
    return $this->subtotal;
  }
}