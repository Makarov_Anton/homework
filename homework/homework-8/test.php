<?php
session_start();
$path = __DIR__ . "/test/";
if(!isset($_GET['name']) || !file_exists($path . $_GET['name'])) {
  header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found', true);
  exit;
}else{
  $test = $_GET['name'];
}

$content = file_get_contents($path . $test);
$date = json_decode($content, true);
$answer = $_POST;
$i = 0;
if (!empty($answer)){
  foreach ($date as $values){
    if ($values['true'] == $answer[$values['name']]) {
      $i++;
    }
  }
  $ball_d = $i + 1;
  $user = (isset($_SESSION['guest'])) ? $_SESSION['guest'] : $_SESSION['user']['login'];
  $text = $user . ", тест пройден!";
  $ball = "Ваша отцена за пройденый тест: " . $ball_d;
  $image = imagecreatetruecolor(522, 738);
  $backcolor = imagecolorallocate($image, 255, 224, 221);
  $textcolor = imagecolorallocate($image, 50, 50, 50);
  $boxFile = __DIR__ . '/img/sert.png';
  $fontFile = __DIR__ . '/img/arial.ttf';
  $imBox = imagecreatefrompng($boxFile);
  imagecopy($image, $imBox, 0, 0, 0, 0, 522, 700);
  imagettftext($image, 20, 0, 120, 330, $textcolor, $fontFile, $text);
  imagettftext($image, 16, 0, 80, 402, $textcolor, $fontFile, $ball);
  header('Content-type: image/png');
  imagepng($image);
  imagedestroy($image);
}
?>

<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <title>address book</title>
  <link rel="stylesheet" href="../../css/new.css">
</head>
<body>

<section>
  <div style="margin: 20px auto; text-align: center; font-weight: bold; font-size: 18px; color: #1e7e34">
    <?php if (isset($_SESSION['user'])){ ?>
    <a href="admin.php" style="float: left;margin-left: 200px;">Загрузить новые тесты</a>
    <?php } ?>
    <a href="list.php" style="margin-left: 150px;">К загруженным тестам!</a>
  </div>
  <form action="" method="POST">
    <?php foreach ($date as $dates) { ?>
    <fieldset>
      <legend><?= $dates['question'] ?></legend>
      <label><input type="radio" name="<?= $dates['name'] ?>" value="<?= $dates['answer1'] ?>"
                    required><?= $dates['answer1'] ?></label>
      <label><input type="radio" name="<?= $dates['name'] ?>"
                    value="<?= $dates['answer2'] ?>"><?= $dates['answer2'] ?>
      </label>
      <label><input type="radio" name="<?= $dates['name'] ?>"
                    value="<?= $dates['answer3'] ?>"><?= $dates['answer3'] ?>
      </label>
      <label><input type="radio" name="<?= $dates['name'] ?>"
                    value="<?= $dates['answer4'] ?>"><?= $dates['answer4'] ?>
      </label>
      <label><input type="radio" name="<?= $dates['name'] ?>"
                    value="<?= $dates['answer5'] ?>"><?= $dates['answer5'] ?>
      </label>
    </fieldset>
    <?php } ?>
    <input type="submit" value="Отправить" id="button" ">
  </form>
  <div style="font-weight: bold; font-size: 14px; color: #1e7e34; margin: 50px;">
    <a href="logout.php">Выйти</a>
  </div>
</section>
</body>
</html>
