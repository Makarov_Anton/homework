<?php
require_once 'functions.php';
$date = time();
if (isset($_SESSION['ban']) == 1){
  $time = $date - $_SESSION['time'];
  if (($date > $_SESSION['time'])) {
    session_unset();
    redirect(index);

  }else{
    $time = $_SESSION['time'] - $date;
    die("Вы заблокированны на {$time} секунд");
  }
}

if (isAuthorized()) {
  redirect('list');
}
$errors = array();
$block_errors = 1;


if (!empty($_POST)) {
  foreach (getUsers() as $user) {
    if ($_POST['login'] == $user['login'] && $_POST['password'] == $user['password']) {
      if (isset($_SESSION['random']) && $_SESSION['random'] != $_POST['norobot']){
        $errors[] = 'Неверно указан код с картинки';
        $block_errors = 0;
        break;
      }
      $_SESSION['user'] = $user;
      redirect('list');
    } elseif (!empty($_POST['login']) && empty($_POST['password'])) {
      $_SESSION['guest'] = $_POST['login'];
      redirect('list');
    }
  }
  if ($block_errors == 1) {
    $errors[] = 'Неверный логин или пароль';
  }
   $_SESSION['captcha'] = (!isset($_SESSION['random'])) ? 0 : $_SESSION['captcha'] + 1;
}


if (isset($_SESSION['captcha']) && $_SESSION['captcha'] > 1){
  $_SESSION['time'] = strtotime("+60 seconds");
  $_SESSION['ban'] = 1;
  die('Вы заблокированны на 1 мин');
}




if ($block_errors == 1) {
  if (isset($_SESSION['random']) && isset($_POST['norobot'])){
    if ($_SESSION['random'] != $_POST['norobot']){
      $errors[] = 'Неверно указан код с картинки';
    }
  }
}

$_SESSION['error'] = (empty($errors)) ? 0 : $_SESSION['error'] + 1;

?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <title>Авторизация</title>
</head>
<body>
<section id="login">
  <div class="container">
    <div class="row">
      <div class="col-xs-4 ">
        <div class="form-wrap">
          <h4 style="text-align: center"><b>Авторизируйтесь</b> или войдите, как <b>Гость</b>, введя только логин.</h4>
          <ul>
            <?php foreach ($errors as $error): ?>
              <li><?= $error ?></li>
            <?php endforeach; ?>
          </ul>
          <form method="POST">
            <div class="form-group">
              <label for="lg" class="sr-only">Логин</label>
              <input type="text" placeholder="Логин" name="login" id="lg" class="form-control" required maxlength="15" minlength="4">
            </div>
            <div class="form-group">
              <label for="key" class="sr-only">Пароль</label>
              <input type="password"  placeholder="Пароль" name="password" id="key" class="form-control" maxlength="15" minlength="4">
            </div>
            <?php if ($_SESSION['error'] > 1){ ?>
            <input style="margin: 10px 90px" class="input" type="text" name="norobot" required>
            <img style="margin: 10px 80px 30px" src="captcha.php">
            <?php } ?>
            <input type="submit" id="btn-login" class="btn btn-custom btn-lg btn-block" value="Войти">
          </form>

          <hr>
        </div>



      </div> <!-- /.col-xs-12 -->

    </div> <!-- /.row -->
  </div> <!-- /.container -->
</section>
</body>
</html>