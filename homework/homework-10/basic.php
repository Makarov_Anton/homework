
<?php
/*
 * 1.Полиморфизм - это когда свойство базового класса может  использовать функции производных классов.
 * Наследование - унаследование одним классом всех свойств и методов другого класса.
 *
 * 2.Абстрактыне классы имеют и методы и свойства от них можно уноследоваться , интерфейсы имеют только методы и больше нужны для диклариролвания операций.
 * 1) Абстрактный класс можно использовать, если вы собираетесь использовать наследование, которое будет обеспечивать общую структуру.
 * 2) Абстрактный класс можно использовать, если вы хотите объявить приватные экземпляры. В интерфейсах, все методы должны быть публичными.
 * 3) Если вы считаете, что в будущем вам понадобится прибегать к добавлению новых методов, тогда абстрактный класс – лучший вариант.
 *       В случае интерфейса вам необходимо будет объявлять новый метод в каждом классе, который реализует ваш интерфейс.
 * 4) Интерфейсы – хороший выбор, если вам известно, что API не будет меняться некоторое время.
 * 5) Также, для реализации «множественного наследования» кроме интерфейсов вы не найдете альтернативных решений.
 *
 * Product - может быть супер классом
 *
 */

interface method
{
  public function getDiscount();
  public function getDescription();

}

abstract class Products
{
  public $price;
  public $category;
  public $discount;
  public $name;

  public function __construct($name, $category, $price, $discount)
  {
    $this->name = $name;
    $this->category = $category;
    $this->price = $price;
    $this->discount = $discount;
  }

  public function getDiscount(){
    if ($this->discount){
      return round($this->price - ($this->price / $this->discount), 2);
    }else{
      return $this->price;
    }
  }
  public function getDescription()
  {
    echo "{$this->name} находится в категории - {$this->category}.<br> Цена с учётом скидки {$this->getDiscount()}<br><br>";
  }
}

class Car extends Products implements method
{
  public $maxSpeed = 200;
  public $carColor = "white";

  public function getDescription()
  {
    parent::getDescription();
    echo "Цвет: {$this->carColor}<br>Максимальная скорость: {$this->maxSpeed}<br><br>";
    }
}

$bmw = new car("BMW", "car", 500000, 10);
$bmw->maxSpeed = 300;
$bmw->carColor = "red";
$bmw->getDiscount();
$bmw->getDescription();

$mercedes = new car("Mercedes", "car", 700000, 20);
$mercedes->maxSpeed = 320;
$mercedes->carColor = "green";
$mercedes->getDiscount();
$mercedes->getDescription();


class Television extends Products implements method
{
  public $antenna = 1;
  public $channels = 50;
}

$panasonic = new Television("Panasonic", "TV", 45000, 50);
$panasonic->getDiscount();
$panasonic->getDescription();
$toshiba = new Television("Toshiba", "TV", 42000, 10);
$toshiba->getDiscount();
$toshiba->getDescription();

class BallpointPen extends Products implements method
{
  public $penColor = "red";
  public $inkColor = "black";
}

$dalsey = new BallpointPen("Dalsey", "pen", 30, 5);
$dalsey->getDiscount();
$dalsey->getDescription();
$lamy = new BallpointPen("Lamy", "pen", 50, 5);
$lamy->getDiscount();
$lamy->getDescription();

class Duck extends Products implements method
{
  public $wings = 2;
  public $color = "black";
}

$duck = new Duck("Duck", "Bird", 550, 15);
$duck->getDiscount();
$duck->getDescription();
$daffiduck = new Duck("DaffiDuck", "Bird", 100000, 0);
$daffiduck->getDiscount();
$daffiduck->getDescription();






