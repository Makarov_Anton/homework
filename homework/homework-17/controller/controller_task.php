<?php
require_once "../model/function.php";
require_once "../model/task.php";
$twig = twig("../");
if (!isset($_SESSION['login'])){
  redirect("controller_login");
}


try
{
  $db = dbConnect();
  $value = "";
  $get = addslash($_GET);

  $sth = $db->prepare("select id from user WHERE login = '{$_SESSION['login']}'");
  $sth->execute();
  $user = $sth->fetch(PDO::FETCH_ASSOC);

  if (isset($get['action'])) {

    switch ($get['action']) {
      case "edit":
        $value = edit($db, $get);
        break;

      case "done":
        $data['is_done'] = done($db, $get);
        break;

      case "delete":
        delete($db, $get);
        break;
    }
  }

  $post = addslash($_POST);

  if (isset($post['description'])) {
    if (strlen($post['description']) < 4) {
      echo "Количество симвлов должно быть больше 3!";
    } else {
      if (isset($get['action']) && $get['action'] == "edit") {
        update ($db, $get, $post);
        $value = "";
        header("Refresh:0, url=controller_task.php");
      }else{
        insert($db, $user, $post);
      }

    }
  }

  if (isset($post['assigned_user_id'])) {
    $assigned_task = explode(";", $post['assigned_user_id']);
    $task_id = $assigned_task[0];
    $assigned_user = $assigned_task[1];
    update_assigned_user($db, $assigned_user, $task_id);

  }

  $sql_author = sql_author();

  $sql_assigned = sql_assigned();

  if (isset($post['sort_by'])) {
    $sql_author = sort_print_task($post, $sql_author);
  }

  $data = print_task($sql_author, $db);
  $assigned_data = print_task($sql_assigned, $db);


  $users = users($db);

  if (isset($_GET['logout']) && $_GET['logout'] == 1) {
    logout();
  }


  $params = ['login' => $_SESSION['login'],
    'data' => $data,
    'assigned_data' => $assigned_data,
    'users' => $users,
    'value' => $value
  ];

  echo $twig->render('task.twig', $params);
}
catch (PDOException $e) {
  die("Error: " . $e->getMessage());
}


