<?php

/* task.twig */
class __TwigTemplate_ea0011aa5735db3746c554b98b360f26cb3137c4c6f9e2a4f8a090690bca965a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!doctype html>
<html lang=\"ru\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\">
    <title></title>

    <style>
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }
    </style>

</head>
<body>

<h1>Добро пожаловать, ";
        // line 28
        echo twig_escape_filter($this->env, ($context["login"] ?? null), "html", null, true);
        echo ". Твой список дел на сегодня.</h1>
<div style=\"float: left\">
    <form method=\"POST\">

        <input type=\"text\" name=\"description\" placeholder=\"Описание задачи\" value=";
        // line 32
        echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
        echo ">
        <input type=\"submit\" name=\"save\" value=\"Добавить\">

    </form>
</div>
<div style=\"float: left; margin-left: 20px;\">
    <form method=\"POST\">
        <label for=\"sort\">Сортировать по:</label>
        <select name=\"sort_by\">
            <option value=\"date_created\">Дате добавления</option>
            <option value=\"is_done\">Статусу</option>
            <option value=\"description\">Описанию</option>
        </select>
        <input type=\"submit\" name=\"sort\" value=\"Отсортировать\"/>
    </form>
</div>
<div style=\"clear: both\"></div>

<table>
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
        <th>Закрепить задачу за пользователем</th>
    </tr>

        ";
        // line 61
        if (twig_test_empty(($context["data"] ?? null))) {
            // line 62
            echo "            <h3> Дел пока нет, кайфуй.</h3>
        ";
        } else {
            // line 64
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["data"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["date"]) {
                // line 65
                echo "                ";
                if (($this->getAttribute($context["date"], "description", array(), "any", true, true) && (twig_length_filter($this->env, $this->getAttribute($context["date"], "description", array())) > 3))) {
                    // line 66
                    echo "
    <tr>
        <td>";
                    // line 68
                    echo twig_escape_filter($this->env, $this->getAttribute($context["date"], "description", array()), "html", null, true);
                    echo "</td>
        <td>";
                    // line 69
                    echo twig_escape_filter($this->env, $this->getAttribute($context["date"], "date_added", array()), "html", null, true);
                    echo "</td>
        <td style=\"color: ";
                    // line 70
                    if (($this->getAttribute($context["date"], "is_done", array()) == 0)) {
                        echo " orange ";
                    } else {
                        echo " green ";
                    }
                    echo "\">
            ";
                    // line 71
                    if (($this->getAttribute($context["date"], "is_done", array()) == 0)) {
                        echo " В процессе ";
                    } else {
                        echo " Выполнено ";
                    }
                    // line 72
                    echo "        </td>
        <td>
            <a href='?id=";
                    // line 74
                    echo twig_escape_filter($this->env, $this->getAttribute($context["date"], "id", array()), "html", null, true);
                    echo "&action=edit'>Изменить</a>
            <a href='?id=";
                    // line 75
                    echo twig_escape_filter($this->env, $this->getAttribute($context["date"], "id", array()), "html", null, true);
                    echo "&action=done'>Выполнить</a>
            <a href='?id=";
                    // line 76
                    echo twig_escape_filter($this->env, $this->getAttribute($context["date"], "id", array()), "html", null, true);
                    echo "&action=delete'>Удалить</a>
        </td>
        <td>";
                    // line 78
                    echo twig_escape_filter($this->env, $this->getAttribute($context["date"], "assigned", array()), "html", null, true);
                    echo "</td>
        <td>";
                    // line 79
                    echo twig_escape_filter($this->env, $this->getAttribute($context["date"], "author", array()), "html", null, true);
                    echo "</td>
        <td>
            <form method=\"POST\">
                <select name=\"assigned_user_id\">
                    ";
                    // line 83
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["users"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                        // line 84
                        echo "                        <option value=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["date"], "id", array()), "html", null, true);
                        echo ";";
                        echo twig_escape_filter($this->env, $context["user"], "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $context["user"], "html", null, true);
                        echo "</option>
                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 86
                    echo "                </select>
                <input type=\"submit\" name=\"assign\" value=\"Переложить ответственность\">
            </form>
        </td>
    </tr>

                ";
                }
                // line 93
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['date'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 94
            echo "        ";
        }
        // line 95
        echo "</table>
<div>
    <h3>Также, посмотрите, что от Вас требуют другие люди:</h3>
</div>

<table>
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
    </tr>
    ";
        // line 109
        if (twig_test_empty(($context["assigned_data"] ?? null))) {
            // line 110
            echo "        <h3> Дел пока нет, кайфуй.</h3>
    ";
        } else {
            // line 112
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["assigned_data"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["dates"]) {
                // line 113
                echo "            ";
                if (($this->getAttribute($context["dates"], "description", array(), "any", true, true) && (twig_length_filter($this->env, $this->getAttribute($context["dates"], "description", array())) > 3))) {
                    // line 114
                    echo "    <tr>
        <td>";
                    // line 115
                    echo twig_escape_filter($this->env, $this->getAttribute($context["dates"], "description", array()), "html", null, true);
                    echo "</td>
        <td>";
                    // line 116
                    echo twig_escape_filter($this->env, $this->getAttribute($context["dates"], "date_added", array()), "html", null, true);
                    echo "</td>
        <td style=\"color: ";
                    // line 117
                    if (($this->getAttribute($context["dates"], "is_done", array()) == 0)) {
                        echo " orange ";
                    } else {
                        echo " green ";
                    }
                    echo "\">
            ";
                    // line 118
                    if (($this->getAttribute($context["dates"], "is_done", array()) == 0)) {
                        echo " В процессе ";
                    } else {
                        echo " Выполнено ";
                    }
                    // line 119
                    echo "        </td>
        <td>
            <a href='?id=";
                    // line 121
                    echo twig_escape_filter($this->env, $this->getAttribute($context["dates"], "id", array()), "html", null, true);
                    echo "&action=edit'>Изменить</a>
            <a href='?id=";
                    // line 122
                    echo twig_escape_filter($this->env, $this->getAttribute($context["dates"], "id", array()), "html", null, true);
                    echo "&action=done'>Выполнить</a>
            <a href='?id=";
                    // line 123
                    echo twig_escape_filter($this->env, $this->getAttribute($context["dates"], "id", array()), "html", null, true);
                    echo "&action=delete'>Удалить</a>
        </td>
        <td>";
                    // line 125
                    echo twig_escape_filter($this->env, ($context["login"] ?? null), "html", null, true);
                    echo "</td>
        <td>";
                    // line 126
                    echo twig_escape_filter($this->env, $this->getAttribute($context["dates"], "author", array()), "html", null, true);
                    echo "</td>
    </tr>
            ";
                }
                // line 129
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dates'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 130
            echo "    ";
        }
        // line 131
        echo "
</table>

<div style=\"font-weight: bold; font-size: 14px; color: #1e7e34; margin: 50px 0;\">
    <a href=\"../controller/controller_task.php?logout=1\">Выйти</a>
</div>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "task.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  281 => 131,  278 => 130,  272 => 129,  266 => 126,  262 => 125,  257 => 123,  253 => 122,  249 => 121,  245 => 119,  239 => 118,  231 => 117,  227 => 116,  223 => 115,  220 => 114,  217 => 113,  212 => 112,  208 => 110,  206 => 109,  190 => 95,  187 => 94,  181 => 93,  172 => 86,  159 => 84,  155 => 83,  148 => 79,  144 => 78,  139 => 76,  135 => 75,  131 => 74,  127 => 72,  121 => 71,  113 => 70,  109 => 69,  105 => 68,  101 => 66,  98 => 65,  93 => 64,  89 => 62,  87 => 61,  55 => 32,  48 => 28,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "task.twig", "D:\\netology\\homework\\homework-17\\templates\\task.twig");
    }
}
