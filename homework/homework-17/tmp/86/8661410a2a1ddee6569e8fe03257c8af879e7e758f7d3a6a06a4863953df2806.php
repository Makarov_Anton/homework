<?php

/* login.twig */
class __TwigTemplate_f88f67f4598797ce1f9108d65d543c198a65914b80140283612a233d9f70315c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"ru\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
</head>
<body>
<p>Введите данные для регистрации или войдите, если уже регистрировались:</p>

<form method=\"POST\">
    <input type=\"text\" name=\"login\" placeholder=\"Логин\" />
    <input type=\"password\" name=\"password\" placeholder=\"Пароль\" />
    <input type=\"submit\" name=\"sign_in\" value=\"Вход\" />
    <input type=\"submit\" name=\"register\" value=\"Регистрация\" />
</form>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "login.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "login.twig", "D:\\netology\\homework\\homework-17\\templates\\login.twig");
    }
}
