<?php

/* login.twig */
class __TwigTemplate_d60fe834b5351b427d17b0fb2d0a5aa5351676966ffc0d047ee8bd559efb71ce extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"ru\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <title>Document</title>
</head>
<body>
<p>Введите данные для регистрации или войдите, если уже регистрировались:</p>

<form method=\"POST\">
    <input type=\"text\" name=\"login\" placeholder=\"Логин\" />
    <input type=\"password\" name=\"password\" placeholder=\"Пароль\" />
    <input type=\"submit\" name=\"sign_in\" value=\"Вход\" />
    <input type=\"submit\" name=\"register\" value=\"Регистрация\" />
</form>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "login.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "login.twig", "D:\\netology\\homework\\homework-17\\templates\\login.twig");
    }
}
