<?php
//phpinfo();
$isbn = "";
$name = "";
$author = "";
define('DB_DRIVER','mysql');
define('DB_HOST','127.0.0.1');
define('DB_NAME','books');
define('DB_USER','mysql');
define('DB_PASS','mysql');
try
{
  function addslash($array) {
    $arr = array();
    foreach ($array as $key => $value){
      $arr[$key] = addslashes($value);
    }
    return $arr;
  }
  $value = "";
  $connect_str = DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_NAME;
  $db = new PDO($connect_str, DB_USER, DB_PASS);

  $get = addslash($_GET);

  if (isset($get['action'])){

    switch ($get['action']){
      case "edit":
        $sqlEdit = ("select description from tasks WHERE id = {$get['id']}");

        if ($res = $db->query($sqlEdit)) {
          foreach ($res as $row) {
            $value = $row['description'];
          }
        }
        break;

      case "done":
        $data['is_done'] = 1;
        $sqlDone = ("update tasks SET is_done = 1 WHERE id = {$get['id']}");
        $db->exec($sqlDone);
        break;

      case "delete":
        $sqlDelete = ("DELETE FROM tasks WHERE id = {$get['id']}");
        $db->exec($sqlDelete);
        break;
    }
  }

  $post = addslash($_POST);


  if (isset($post['description'])) {
    if (strlen($post['description']) < 4){
      echo "Количество симвлов должно быть больше 3!";
    }else{
      if (isset($get['action']) == "edit"){
        $sqlDelete = ("DELETE FROM tasks WHERE description = '{$value}'");
        $db->exec($sqlDelete);
        $value = "";
      }
      $sqlInsert = ("INSERT INTO tasks (description, date_added) VALUE ('{$post['description']}', NOW())");
      $db->exec($sqlInsert);
    }

  }

  if (isset($post['sort_by'])){
    switch ($post['sort_by']){
      case "date_created":

        $sql = ("select * from tasks ORDER BY date_added");
        break;
      case "is_done":
        $sql = ("select * from tasks ORDER BY is_done");
        break;
      case "description":
        $sql = ("select * from tasks ORDER BY description");
        break;
    }
  }else{
    $sql = ("select * from tasks");
  }

  if ($result = $db->query($sql)) {
    foreach ($result as $row) {
      $data[] = array(
        "id" => $row['id'],
        "description" => $row['description'],
        "is_done" => $row['is_done'],
        "date_added" => $row['date_added']
      );
    }
  }
}

catch(PDOException $e)
{
  die("Error: ".$e->getMessage());
}

?>

<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
  <title></title>

  <style>
    table {
      border-spacing: 0;
      border-collapse: collapse;
    }

    table td, table th {
      border: 1px solid #ccc;
      padding: 5px;
    }

    table th {
      background: #eee;
    }
  </style>

</head>
<body>

<h1>Список дел на сегодня</h1>
<div style="float: left">
  <form method="POST">

    <input type="text" name="description" placeholder="Описание задачи" value="<?= $value?>" >
    <input type="submit" name="save" value="Добавить" >
  </form>
</div>
<div style="float: left; margin-left: 20px;">
  <form method="POST">
    <label for="sort">Сортировать по:</label>
    <select name="sort_by">
      <option value="date_created">Дате добавления</option>
      <option value="is_done">Статусу</option>
      <option value="description">Описанию</option>
    </select>
    <input type="submit" name="sort" value="Отсортировать" />
  </form>
</div>
<div style="clear: both"></div>

<table>
  <tr>
    <th>Описание задачи</th>
    <th>Дата добавления</th>
    <th>Статус</th>
    <th></th>
  </tr>
  <tr>

    <?php
      if (empty($data)){
        echo "Дел пока нет, кайфуй.";
      }else{
      foreach ($data as $date){
        $status = ($date['is_done'] == 0) ? "<span style='color: orange;'>В процессе</span>" : "<span style='color: green;'>Выполнено</span>";
    ?>
    <?php if (isset($date['description']) && strlen($date['description']) > 3){?>
  <tr>
    <td><?= $date['description']?></td>
    <td><?= $date['date_added']?></td>
    <td><?= $status?></td>
    <td>
        <a href='?id=<?= $date['id']?>&action=edit'>Изменить</a>
        <a href='?id=<?= $date['id']?>&action=done'>Выполнить</a>
        <a href='?id=<?= $date['id']?>&action=delete'>Удалить</a>
    </td>
  </tr>
  </tr>
  <?php }}} ?>

</table>
</body>
</html>
